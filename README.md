# Тестовое задание для uniquesystems

## Репозитории

  * [blog](https://github.com/mrsutter/blog) - тестовое задание(админская учетка на heroku: yakspavel01blog.gmail.com/123456)
  * [task_manager](https://github.com/mrsutter/task_manager) - рельсовая апа
  * [url_inspector](https://github.com/mrsutter/url_inspector) - pure ruby

## Ответы на вопросы

### Обработка ошибок
  * Преимущества: мощный инструмент, удобная иерархическая структура, легко ловить и создавать собственные исключения
  * Недостатки: с той же легкостью можно перехватить то, что не нужно - например, системные ошибки.

### Дебаггинг
  Мне обычно хватает:

  * [bullet](https://github.com/flyerhzm/bullet)
  * [rack-mini-profiler](https://github.com/MiniProfiler/rack-mini-profiler)
  * [pry-rails](https://github.com/rweng/pry-rails)
  * [better_errors](https://github.com/charliesome/better_errors)
  * [meta_request](https://github.com/dejan/rails_panel/tree/master/meta_request) + плагин для хрома
  * [web-console](https://github.com/rails/web-console)

### Используемые гемы
  Их много. Например мне нравится [shamrock](https://github.com/jsl/shamrock). Если, у Вас в приложении много апи-клиентов, вынесенных в гемы, гемы также будут вовлечены в процесс тестирования. Это очень удобный централизованный stub.

### Процессы и потоки
  Процесс - программа в режиме выполнения, с которым связано его адресное пространство. В одном процессе могут существовать несколько потоков, разделяющих ресурсы процесса. К плюсам потоков можно отнести: гораздо меньшее время для создания, чем у процесса, меньше времени для переключения контекста, данные одного потока сразу становятся сразу доступными для всех остальных.

  Недостатки потоков хорошо сформулировал создатель Nginx Игорь Сысоев: 1) более трудоёмкое программирование 2) процессы обеспечивают естественную изоляцию ресурсов, уменьшая конкуренцию за них.

  В ruby ближайший пример - это puma (на потоках) и unicorn(процессы). Работать с unicorn проще, не надо заботиться о thread-safe, но, например, при большом количестве сложных sql запросов, unicorn может съесть непозволительно много памяти.

### Асинхронный Ruby

  * [eventmachine](https://github.com/eventmachine/eventmachine)
  * [concurrent-ruby](https://github.com/ruby-concurrency/concurrent-ruby) (например, как в [url_inspector](https://github.com/mrsutter/url_inspector))
  * [daemons](https://github.com/thuehlinger/daemons)
  * Гемы, добавляющие задачи в крон, например [whenever](https://github.com/javan/whenever)
  * ActiveJob, Sidekiq, Resque, etc

Если абстрактно, то могу рассказать, как работает eventloop в javascript: вызов каждой функции создает контекст исполнения, при вызове вложенной функции создается новый контекст и помещается в стек перед первой. Когда стек пустеет, следующее событие извлекается из очереди(события, подлежащие обработке, связанное с вызовом функции) и обрабатывается.


### Формат общения с клиентом
  JSON - простота, компактность и легкость обработки данных на стороне клиента
